package com.zy;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zy.Bean.SignImgKeywordInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * @Author James
 * @Date 2024/04/24 14:21
 **/
@SpringBootTest
public class TEST {
    private static final Logger logger = LogManager.getLogger(Test.class);

    @Autowired
    private NamedParameterJdbcOperations JdbcStatement;
    @Test
    void transJsonToMap() throws IOException {
        File file = ResourceUtils.getFile("classpath:static/sign_member.json");
        String jsonStr = readerMethod(file);
        String[] signArr = new String[]{"许总","严总"};
        Map mapTypes = JSON.parseObject(jsonStr);
        List<SignImgKeywordInfo> signList = new ArrayList<>();
        mapTypes.forEach(new BiConsumer() {
            @Override
            public void accept(Object key, Object value) {
                for (String name : signArr) {
                    if (name.equals(key)){
                        System.out.println(value);
                        SignImgKeywordInfo signInfo = JSONObject.parseObject(value.toString(), SignImgKeywordInfo.class);
                        signInfo.setName(key.toString());
                        signList.add(signInfo);
                        break;
                    }
                }
            }
        });

        System.out.println(signList);


    }


    private static String readerMethod(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        Reader reader = new InputStreamReader(new FileInputStream(file), "UTF-8");
        int ch= 0;
        StringBuffer sb = new StringBuffer();
        while((ch = reader.read()) != -1) {
            sb.append((char) ch);
        }
        fileReader.close();
        reader.close();
        String jsonStr = sb.toString();
        System.out.println(JSON.parseObject(jsonStr));
        return jsonStr;
    }


    @Test
    public void ts(){
        String sql = "SELECT 1 FROM flow_run WHERE run_id = :runId AND current_step = 0";
        Map<String, Object> params = new HashMap<>();
        params.put("runId",27);
        // 执行动态SQL
        JdbcStatement.query(sql, params, resultSet -> {
            System.out.println(resultSet.getString(1));
            System.out.println(resultSet.next());
        });
    }



}
