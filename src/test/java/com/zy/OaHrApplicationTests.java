package com.zy;

import com.zy.service.AttachmentService;
import com.zy.service.SDKService;
import com.zy.utils.Base64;
import com.zy.vo.base.Result;
import com.zy.vo.request.CertEventRequest;
import com.zy.vo.request.DocumentSignRequest;
import com.zy.vo.response.CertEventResponse;
import com.zy.vo.response.DocumentSignResponse;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@SpringBootTest
class OaHrApplicationTests {
    @Autowired
    private SDKService sdkService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private NamedParameterJdbcOperations JdbcStatement;
    @Test
    void contextLoads() throws IOException {

        byte [] pdf = FileUtils.readFileToByteArray(new File("E://1.pdf"));

        byte [] png =  FileUtils.readFileToByteArray(doWhite(new File("C://Users//Administrator//Desktop//签名 (2).png")));

        CertEventRequest certEventRequest = new CertEventRequest();

        certEventRequest.setCertPassword("123456");

        certEventRequest.setCertSubject("Q");

        String uuid = UUID.randomUUID().toString();
        certEventRequest.setUniqueCode(uuid);
        Result<CertEventResponse> result =  sdkService.certEvent(certEventRequest);

        DocumentSignRequest docRequest = new DocumentSignRequest();
        docRequest.setUniqueCode(uuid);

        docRequest.setCertPassword("123456");
        docRequest.setPfx(result.getData().getPfx());
        docRequest.setDocumentFile(Base64.encode(pdf));

        docRequest.setSignType(1);
        docRequest.setKeywords("严长志签名");
        docRequest.setSignatureFile(Base64.encode(png));

   //     byte [] signPDF = sdkService.chopStampSign(pdf, Base64.decode(result.getData().getPfx()),"123456",png);
        Result<DocumentSignResponse> resp = sdkService.documentSign(docRequest);
        byte [] signPDF =  resp.getData().getDocumentFile();
        System.out.println(resp.getCode()+"  " + resp.getMessage());
        FileUtils.writeByteArrayToFile(new File("E://chop.pdf"),signPDF);

        //       FileUtils.writeByteArrayToFile(new File("E://chop.pdf"),signPDF);
    }

    public static File doWhite(File file) throws IOException {

        // 读取图片
       BufferedImage image = null;
        //  BufferedImage image = cropImage(ImageIO.read(file));
        try {

    //        image = cropImage(ImageIO.read(file));

            image = ImageIO.read(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (image != null) {
            // 将白色变成透明
            makeWhiteTransparent(image);

            // 保存处理后的图片
            try {
                File output = new File("C://Users//Administrator//Desktop//output.png");
                ImageIO.write(image, "PNG", output);
                return output;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;

    }


    public static BufferedImage cropImage(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        int startY = 0;
        // 从上往下扫描，找到第一个出现黑色像素的行
        for (int y = 0; y < height; y++) {
            boolean hasBlackPixel = false;
            for (int x = 0; x < width; x++) {
                int pixel = image.getRGB(x, y);
                if (pixel == 0xFF000000) { // 判断是否为黑色像素
                    hasBlackPixel = true;
                    break;
                }
            }
            if (hasBlackPixel) {
                startY = y;
                break;
            }
        }

        int newHeight = height - startY;

        BufferedImage croppedImage = new BufferedImage(width, newHeight, image.getType());
        Graphics2D g2d = croppedImage.createGraphics();
        g2d.drawImage(image, 0, 0, width, newHeight, 0, startY, width, height, null);
        g2d.dispose();

        return croppedImage;
    }

    private static void makeWhiteTransparent(BufferedImage image) {
        // 获取图片宽高
        int width = image.getWidth();
        int height = image.getHeight();

        // 遍历像素点
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // 获取当前像素点的颜色
                int rgba = image.getRGB(x, y);
                Color color = new Color(rgba, true);

                // 如果是白色，则将其设为透明
                if (color.getRed() == 255 && color.getGreen() == 255 && color.getBlue() == 255) {
                    image.setRGB(x, y, 0x00FFFFFF); // 将白色改成透明
                }
            }
        }

    }


    private static File makeImgWH(File file){
        try {
            // 读取原始图片
            BufferedImage originalImage = ImageIO.read(file);

            // 设置目标图片的宽度
   //         int targetWidth = 600;  // 任意值
            // 计算目标图片的高度，保持宽高比为15:7
      //      int targetHeight = (int) Math.round((double) targetWidth * 7 / 15);
            int targetWidth = 600;
            int targetHeight = 400;
            // 创建目标图片
            BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);

            // 使用 Graphics2D 进行绘制
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(originalImage, 50, 100, targetWidth, targetHeight, null);
            g.dispose();

            File output = new File("C://Users//Administrator//Desktop//output.png");
            // 将目标图片保存到文件
            ImageIO.write(resizedImage, "png", output);
            System.out.println("图片调整完成。");
            return output;
        } catch (Exception e) {
            System.out.println("发生异常: " + e.getMessage());
        }
        return null;
    }




    @Test
    void TestSth(){
        String date = "2024-04-10";
        String foldName = "sadsafx123sdf";
        StringBuffer fixedPath = new StringBuffer("D://E-office_Server//attachment//");
        date = date.replace("-","//");
        fixedPath.append(date);
        fixedPath.append("//");
        fixedPath.append(foldName);

        System.out.println(fixedPath.toString());

    }

    @Test
    void TestSth2(){
        String date = "2024-04-10";
        String foldName = "sadsafx123sdf";

        changeString(date,foldName);

        System.out.println(date);
        System.out.println(foldName);
    }


    public void changeString(String a,String b){
        a = "1";
        b = "2";
    }


    @Test
    void doJJBC(){

        String attachmentTableName = "attachment_2024_5";

        StringBuffer path = new StringBuffer();
        path.append("D:/E-office_Server/attachment/");
//        String sql = "SELECT attachment_path,affect_attachment_name FROM " + attachmentTableName +
//                " WHERE attachment_path LIKE :attachment_path";
         String sql = "SELECT 1 FROM " + attachmentTableName +
       " WHERE attachment_name LIKE '%签名%'";
        Map<String, Object> params = new HashMap<>();
//        params.put("attachment_path", "%a8c4177dd1a65ca4aadc162a754cfdd4%");
        // 执行动态SQL
        JdbcStatement.query(sql, params, resultSet -> {
            // 处理结果集
            System.out.println(resultSet.getString(1));

        });

        System.out.println(path.toString());
    }


}
