package com.zy.exception;


import com.zy.enums.APIResultEnum;
import com.zy.enums.ParamFormatErrorEnum;

/**
 * @Description: PaasException
 * @Package: org.resrun.exception
 * @ClassName: PaasException
 * @author: FengLai_Gong
 */
public class ParamFormatException extends RuntimeException{

    protected final transient APIResultEnum resultEnum ;

    protected final transient ParamFormatErrorEnum paramFormatErrorEnum ;


    public ParamFormatException(APIResultEnum resultEnum, ParamFormatErrorEnum paramFormatErrorEnum){
        this.resultEnum = resultEnum ;
        this.paramFormatErrorEnum = paramFormatErrorEnum;
    }





}