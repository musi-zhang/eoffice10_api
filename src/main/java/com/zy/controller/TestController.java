package com.zy.controller;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zy.Bean.Flow;
import com.zy.Bean.UserInfo;
import com.zy.Bean.SignImgKeywordInfo;
import com.zy.service.AttachmentService;
import com.zy.service.DocumentSignService;
import com.zy.service.FlowService;
import com.zy.service.impl.DocumentSignServiceImpl;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.BiConsumer;

/**
 * @Author James
 * @Date 2024/03/18 16:00
 **/
@RestController
@RequestMapping("api")
public class TestController {


    private static final Logger logger = LogManager.getLogger(DocumentSignServiceImpl.class);

    @Autowired
    private DocumentSignService documentSignService;

    @Autowired
    private AttachmentService atService;

    @Autowired
    private FlowService flowService;

    @PostMapping("/one")
    private String getString(@RequestParam Map<String, String> allParams){
        System.out.println(allParams);
        return "Hello";
    }

    @PostMapping(value = "/sign", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public void handleFormUrlEncodedRequest(@RequestParam Map<String, String> allParams) throws IOException {

        String req_num = allParams.get("DATA_155");
        if (Objects.isNull(req_num)){
            logger.info("接口调用异常：非正常调用接口");
            return;
        }

        logger.info("附件签名接口开始调用，流水号："+req_num);
        Integer run_id = Integer.valueOf(allParams.get("run_id"));
        //通过run_id 查询流程是否结束
        boolean finishFlow = flowService.isFinishFlow(run_id);

        if (!finishFlow)  {
         logger.info("OA系统异常，流程尚未结束触发数据外发");
         logger.info("接口停止调用。。。。。。。");
         return;
        }


        ClassPathResource resource = new ClassPathResource("other_control.json");
        InputStream inputStream = resource.getInputStream();
        File tempFile = File.createTempFile("temp", ".json");
        FileUtils.copyInputStreamToFile(inputStream, tempFile);

        String other_controlPath = tempFile.getAbsolutePath();

        File file = ResourceUtils.getFile(other_controlPath);
        String jsonStr = readerMethod(file);
        Map mapTypes = JSON.parseObject(jsonStr);

        /**
         * @Params:
         * 1、attachment_table 附件表名  2、PDF名字  3、用户id 4、年月日
         */
        String attachment_table = allParams.get(mapTypes.get("attachment_table"));
        String[] pdfNames = allParams.get(mapTypes.get("pdf_name")).trim().split(",");
        List<String> pdfPaths = new ArrayList<>();
        for (String pdfName : pdfNames) {
            String pdfPath = atService.getAttachmentPath(attachment_table, pdfName);
            if (!Objects.isNull(pdfPath)){
                pdfPaths.add(pdfPath);
            }
        }

        if (pdfPaths.isEmpty()){
            logger.info("当前流程没有需要签名的文件");
            logger.info("接口停止调用。。。。。。。");
            logger.info("流水号："+req_num);
            return;

        }




        String[] signMembers = allParams.get(mapTypes.get("sign_members")).split(",");

        logger.info("PDFPaths "+pdfPaths);

        logger.info("other_control.json 解析完毕  内容是");
        logger.info(mapTypes);

        ClassPathResource resource2 = new ClassPathResource("sign_member.json");

        InputStream inputStream2 = resource2.getInputStream();
        File tempFile2 = File.createTempFile("temp2", ".json");
        FileUtils.copyInputStreamToFile(inputStream2, tempFile2);

        String sign_controlPath = tempFile2.getAbsolutePath();
        File file2 = ResourceUtils.getFile(sign_controlPath);
        String jsonStr2 = readerMethod(file2);
        Map mapTypes2 = JSON.parseObject(jsonStr2);
        List<SignImgKeywordInfo> signList = new ArrayList<>();
        mapTypes2.forEach(new BiConsumer() {
            @Override
            public void accept(Object key, Object value) {
                for (String name : signMembers) {
                    if (name.equals(key)){
                        SignImgKeywordInfo signInfo = JSONObject.parseObject(value.toString(), SignImgKeywordInfo.class);
                        signInfo.setName(key.toString());
                        getSignImgNKeyword(signInfo,allParams);
                        signList.add(signInfo);
                        break;
                    }
                }
            }
        });

        logger.info("sign_member.json 解析完毕  内容是");
        logger.info(mapTypes2);

        logger.info("签名人员信息："+signList);

        int j = 1;
        for (String pdfPath : pdfPaths) {
            int i = 1;
            for (SignImgKeywordInfo simf : signList) {
                logger.info("第 "+j+"张PDF的"+"第"+i+"位人员开始");
                logger.info("人员信息："+simf.toString());
                documentSignService.documentHandle(pdfPath,simf.getPngPath(),simf.getKeyword());
                i++;
            }
            j++;
        }

        logger.info("附件签名接口结束，流水号："+req_num);

    }



    @PostMapping(value = "/getmessage", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public void getMessage(@RequestParam Map<String, String> allParams){

        BlMap(allParams);

    }

    public Flow getFlow(Map<String, String> allParams){

        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(allParams.get("userInfo[user_id]"));
        userInfo.setUserName(allParams.get("userInfo[user_name]"));
        userInfo.setUserAccounts(allParams.get("userInfo[user_accounts]"));
        userInfo.setDeptName(allParams.get("userInfo[dept_name]"));
        userInfo.setDeptId(allParams.get("userInfo[dept_id]"));
        userInfo.setRoleName(allParams.get("userInfo[role_name]"));
        userInfo.setRoleId(allParams.get("userInfo[role_id]"));

        Flow flow = new Flow();
        flow.setId(allParams.get("id"));
        flow.setRunId(allParams.get("run_id"));
        flow.setOutSendId(allParams.get("outsend_id"));
        flow.setRunName(allParams.get("run_name"));
        flow.setFlowId(allParams.get("flow_id"));
        flow.setNodeId(allParams.get("node_id"));
        flow.setFormId(allParams.get("form_id"));
        flow.setProcessName(allParams.get("process_name"));
        flow.setUserInfo(userInfo);

        return flow;
    }

    public void BlMap(Map<String,String> map){
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println("Key: " + key + ", Value: " + value);
        }
    }

    public String getOaFilePath(String foldName,String date){

        //StringBuffer folderPath = new StringBuffer("E:/E-office_back/");
        StringBuffer folderPath = new StringBuffer("D:/Eoffice_Server/attachment/");
        // 创建一个日期时间格式化器，用于解析和格式化日期时间
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        // 解析年月日时分秒的字符串为 LocalDateTime 对象
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);

        // 将 LocalDateTime 对象格式化为年月日格式的字符串
        String dateString = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        dateString = dateString.replace("-","//");
        folderPath.append(dateString);
        folderPath.append("//");
        folderPath.append(foldName);
        String pdfPath = null;
        File folder = new File(folderPath.toString());
        if (folder.exists() && folder.isDirectory()) {
            File[] files = folder.listFiles();
            if (files != null) {
                pdfPath = files[0].getAbsolutePath().replace("\\","//");
            }
        }
        return pdfPath;
    }

    //签名关键字集合
    public List<String> getKeywords(String keywords){

        List keywordsList = new ArrayList();

        if (Objects.isNull(keywords)) return null;

        keywordsList = Arrays.asList(keywords.split(","));

        return keywordsList;

    }


    public void getSignImgNKeyword(SignImgKeywordInfo siif,Map<String,String> allParams){

        logger.info(siif.toString());
        String keyword = null;
        String pngPath = null;

        String foldName = allParams.get(siif.getPngFoldName());
        String countersign_time = allParams.get(siif.getPngTime());
        keyword = allParams.get(siif.getKeywordControl()).trim().toUpperCase();
        pngPath = getOaFilePath(foldName,countersign_time);

        siif.setKeyword(keyword);
        siif.setPngPath(pngPath);

    }


    private static String readerMethod(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        Reader reader = new InputStreamReader(new FileInputStream(file), "UTF-8");
        int ch= 0;
        StringBuffer sb = new StringBuffer();
        while((ch = reader.read()) != -1) {
            sb.append((char) ch);
        }
        fileReader.close();
        reader.close();
        String jsonStr = sb.toString();
        return jsonStr;
    }

}
