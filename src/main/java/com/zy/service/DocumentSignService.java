package com.zy.service;

import org.springframework.stereotype.Service;

/**
 * @Author James
 * @Date 2024/04/17 10:54
 **/

public interface DocumentSignService {

    void documentHandle(String pdfPath,String pngPath,String keyword);

}
