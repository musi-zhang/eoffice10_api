package com.zy.service.impl;

import com.zy.service.AttachmentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Author James
 * @Date 2024/04/19 10:45
 **/
@Service
public class AttachmentServiceImpl implements AttachmentService {

    private static final Logger logger = LogManager.getLogger(DocumentSignServiceImpl.class);

    @Autowired
    private NamedParameterJdbcOperations JdbcStatement;

    @Override
    public String getAttachmentPath(String attachmentTableName,String affect_attachment_name ) {

        AtomicBoolean flag = new AtomicBoolean(false);
        logger.info("attachmentTableName pdfName, userId, date",attachmentTableName,affect_attachment_name);
        String _aaname = "%"+affect_attachment_name+"%";

        Map<String, Object> params = new HashMap<>();
        params.put("attachment_path", _aaname);

        StringBuffer path = new StringBuffer();

        //path.append("E:/E-office_back/");
        path.append("D:/Eoffice_Server/attachment/");
        String sql = "SELECT attachment_path,affect_attachment_name FROM " + attachmentTableName +
                " WHERE attachment_path LIKE :attachment_path AND attachment_name LIKE '%需要签名%'";

        // 执行动态SQL
        JdbcStatement.query(sql, params, resultSet -> {
                 flag.set(true);
            // 处理结果集
                 path.append(resultSet.getString(1));
                 path.append(resultSet.getString(2));
        });
        if (!flag.get()){
            return null;
        }

        String result = path.toString().replace("/","//");
        return result;

    }
}
