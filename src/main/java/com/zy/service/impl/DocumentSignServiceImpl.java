package com.zy.service.impl;

import com.zy.service.DocumentSignService;
import com.zy.service.SDKService;
import com.zy.utils.Base64;
import com.zy.vo.base.Result;
import com.zy.vo.request.CertEventRequest;
import com.zy.vo.request.DocumentSignRequest;
import com.zy.vo.response.CertEventResponse;
import com.zy.vo.response.DocumentSignResponse;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

/**
 * @Author James
 * @Date 2024/04/17 10:56
 **/
@Service
public class DocumentSignServiceImpl implements DocumentSignService {

    private static final Logger logger = LogManager.getLogger(DocumentSignServiceImpl.class);

    @Autowired
    private SDKService sdkService;

    @Override
    public void documentHandle(String pdfPath, String pngPath, String keyword) {

        logger.info("pdfPath： "+pdfPath + "  pngPath："+pngPath+"  keyword："+keyword);
        //构建签名参数
        CertEventRequest certEventRequest = new CertEventRequest();
        certEventRequest.setCertPassword("123456");
        certEventRequest.setCertSubject("张三@123456");
        String uuid = UUID.randomUUID().toString();
        certEventRequest.setUniqueCode(uuid);
        Result<CertEventResponse> result =  sdkService.certEvent(certEventRequest);

        File pdfFile = null;
        byte [] pdf = null;
        File pngFile = null;
        File tempPng = null;
        byte [] png = null;
        byte [] signPDF = null;

        try{
            pdfFile = new File(pdfPath);
            pdf = FileUtils.readFileToByteArray(pdfFile);
            pngFile = new File(pngPath);
            tempPng = doWhite(pngFile);
            png =  FileUtils.readFileToByteArray(tempPng);

            if (!Objects.isNull(pdf)&&!Objects.isNull(png)&&!Objects.isNull(keyword)){
                DocumentSignRequest docRequest = new DocumentSignRequest();
                docRequest.setUniqueCode(uuid);
                docRequest.setCertPassword("123456");
                docRequest.setPfx(result.getData().getPfx());
                docRequest.setDocumentFile(Base64.encode(pdf));
                docRequest.setSignType(1);
                docRequest.setKeywords(keyword);
                docRequest.setSignatureFile(Base64.encode(png));
                Result<DocumentSignResponse> resp = sdkService.documentSign(docRequest);
                logger.info("状态码："+resp.getCode()+" 结果："+resp.getMessage());
                if (resp.getCode().equals(10000)){
                    signPDF =  resp.getData().getDocumentFile();
                    FileUtils.writeByteArrayToFile(new File(pdfPath),signPDF);
                    logger.info("======附件签名成功======");
                }else {
                    logger.info("======附件签名失败======");
                    logger.info("原因："+resp.getMessage());
                }
            }
        }catch (Exception e){
            logger.info(e);
            e.printStackTrace();
        }
        finally {
            if (tempPng.exists()){
                tempPng.delete();
            }
        }


    }




//将白色背景色变透明

    public static File doWhite(File file){
        String uuid = UUID.randomUUID().toString();
        // 读取图片
        BufferedImage image = null;
        try {
            image = ImageIO.read(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (image != null) {
            // 将白色变成透明
            makeWhiteTransparent(image);

            // 保存处理后的图片
            try {
                File output = new File("C://Users//Administrator//Desktop//"+uuid+".png");
                ImageIO.write(image, "PNG", output);
                System.out.println("处理完成，输出文件：" + output.getAbsolutePath());
                return output;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;

    }



    private static void makeWhiteTransparent(BufferedImage image) {
        // 获取图片宽高
        int width = image.getWidth();
        int height = image.getHeight();

        // 遍历像素点
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                // 获取当前像素点的颜色
                int rgba = image.getRGB(x, y);
                Color color = new Color(rgba, true);

                // 如果是白色，则将其设为透明
                if (color.getRed() == 255 && color.getGreen() == 255 && color.getBlue() == 255) {
                    image.setRGB(x, y, 0x00FFFFFF); // 将白色改成透明
                }
            }
        }

    }
}
