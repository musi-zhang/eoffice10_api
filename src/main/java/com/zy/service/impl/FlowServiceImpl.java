package com.zy.service.impl;

import com.zy.service.FlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Author James
 * @Date 2024/05/28 14:15
 **/
@Service
public class FlowServiceImpl implements FlowService {

    @Autowired
    private NamedParameterJdbcOperations JdbcStatement;
    @Override
    public boolean isFinishFlow(Integer runId) {
        AtomicBoolean flag = new AtomicBoolean(false);

        String sql = "SELECT 1 FROM flow_run WHERE run_id = :runId AND current_step = 0";
        Map<String, Object> params = new HashMap<>();
        params.put("runId",runId);
        // 执行动态SQL
        JdbcStatement.query(sql, params, resultSet -> {
                flag.set(true);
        });


        return flag.get();
    }
}
