package com.zy.service;

/**
 * @Author James
 * @Date 2024/05/28 14:14
 **/
public interface FlowService {

    boolean isFinishFlow(Integer runId);

}
