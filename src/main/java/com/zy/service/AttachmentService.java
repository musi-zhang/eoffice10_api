package com.zy.service;

/**
 * @Author James
 * @Date 2024/04/19 10:44
 **/
public interface AttachmentService {

    public String getAttachmentPath(String attachmentTableName,String affect_attachment_name);

}
