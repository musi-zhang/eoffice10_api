package com.zy.Bean;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Author James
 * @Date 2024/04/17 16:02
 **/

@AllArgsConstructor
@NoArgsConstructor
public class SignImgKeywordInfo {

    private String name;
    private String keywordControl;
    private String signControl;
    private String pngPath;
    private String keyword;

    private String pngFoldName;
    private String pngTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeywordControl() {
        return keywordControl;
    }

    public void setKeywordControl(String keywordControl) {
        this.keywordControl = keywordControl;
    }

    public String getSignControl() {
        return signControl;
    }

    public void setSignControl(String signControl) {
        this.signControl = signControl;
    }

    public String getPngPath() {
        return pngPath;
    }

    public void setPngPath(String pngPath) {
        this.pngPath = pngPath;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getPngFoldName() {
        return signControl+"[0][countersign_hand_writing]";
    }

    public void setPngFoldName(String pngFoldName) {
        this.pngFoldName = pngFoldName;
    }

    public String getPngTime() {
        return signControl+"[0][countersign_time]";
    }

    public void setPngTime(String pngTime) {
        this.pngTime = pngTime;
    }

    @Override
    public String toString() {
        return "SignImgKeywordInfo{" +
                "name='" + name + '\'' +
                ", keywordControl='" + keywordControl + '\'' +
                ", signControl='" + signControl + '\'' +
                ", pngPath='" + pngPath + '\'' +
                ", keyword='" + keyword + '\'' +
                ", pngFoldName='" + pngFoldName + '\'' +
                ", pngTime='" + pngTime + '\'' +
                '}';
    }
}
