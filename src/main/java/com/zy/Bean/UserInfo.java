package com.zy.Bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author James
 * @Date 2024/03/19 16:03
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
    private String userId;
    private String userName;
    private String userAccounts;
    private String deptName;
    private String deptId;
    private String roleName;
    private String roleId;
}
