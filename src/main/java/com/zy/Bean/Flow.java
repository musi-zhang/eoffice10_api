package com.zy.Bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author James
 * @Date 2024/03/19 15:58
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Flow implements Serializable {

    private String id;
    private String runId;
    private String outSendId;
    /**
     * 流程标题
     */
    private String runName;

    private String flowId;
    private String nodeId;
    private String formId;
    private String processName;

    private UserInfo userInfo;

}
